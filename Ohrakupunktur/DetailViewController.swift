import UIKit
import AVKit

class DetailViewController: UIViewController {
  
    var videoPlay:AVPlayer?
    
    @IBOutlet weak var nameLabelTextLabel: UILabel!
  
    @IBOutlet weak var myImageView: UIImageView!
  
    @IBOutlet weak var punktZahleLabel: UILabel!
    
    @IBOutlet weak var beschreibungTextView: UITextView!
    
    @IBOutlet weak var grundRezeptTextView: UITextView!
    

    @IBOutlet weak var rezeptmodifikationenTextView: UITextView!
    
    @IBAction func videoInfoButton(_ sender: Any) {
        playLocalVideo()
        
    }
    
    func playLocalVideo(){
        guard let okDetailIndications=detailIndications  else {return}
        if let path=Bundle.main.path(forResource: "\(String(describing: okDetailIndications.name))", ofType: "mp4"){
            let video=AVPlayer(url: URL(fileURLWithPath: path))
            let videoPlay=AVPlayerViewController()
            videoPlay.player=video
            
            present(videoPlay, animated: true, completion: {
                
                video.play()
            })
        }
    }
    
    var detailIndications: Indications? {
        
    didSet {
      configureView()
          }
    }
  func configureView() {
    if let detailIndications = detailIndications {
         if let titlelName = nameLabelTextLabel,
            let myImageView = myImageView ,
            let punktZahleLabel=punktZahleLabel,
            let grundRezept=grundRezeptTextView,
            let rezeptmodifikationen=rezeptmodifikationenTextView,
            let beschreibungTextView=beschreibungTextView {
            
        titlelName.text = detailIndications.name
        myImageView.image = UIImage(named: detailIndications.name)
        punktZahleLabel.text=detailIndications.numOfPoint
        grundRezept.text=detailIndications.grundRezept
        rezeptmodifikationen.text=detailIndications.rezeptModifikationen
        beschreibungTextView.text=detailIndications.beschreibung
            title = detailIndications.name
      }
    }
  }
    func addTapGestureToMyImg(){
        let tapGesture=UITapGestureRecognizer(target: self, action: #selector(handleIntoBigViewImg))
        myImageView.addGestureRecognizer(tapGesture)
        myImageView.isUserInteractionEnabled=true
    }
    @objc func handleIntoBigViewImg(){
        print("cklik")
        performSegue(withIdentifier: "gotobigimg", sender: detailIndications?.name)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotobigimg"{
            if let bigImgViewController=segue.destination as? BigImgViewController{
                bigImgViewController.infoFromDetailVC = sender as? String
            }
        }
    }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureView()
    addTapGestureToMyImg()

  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}

