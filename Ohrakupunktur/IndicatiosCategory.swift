import Foundation

struct Indications {
    var category:String
    var name: String
    var numOfPoint: String
    var beschreibung: String
    var grundRezept: String
    var rezeptModifikationen: String
    
}
