import UIKit

class MasterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var searchFooter: SearchFooter!
  
  var detailViewController: DetailViewController? = nil
  var indication = [Indications]()
  var filteredIndications = [Indications]()
  var section1=[Section1Index]()
  let searchController = UISearchController(searchResultsController: nil)
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationController?.navigationBar.isTranslucent = true
    
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Suchen Sie nach Indikationen"
    searchController.searchBar.barTintColor=UIColor.white
    navigationItem.searchController = searchController
    definesPresentationContext = true
    searchController.searchBar.delegate = self
//    tableView.tableHeaderView = searchFooter
    
    
    searchController.searchBar.scopeButtonTitles = ["All", "Verdauungssystem", "Atmungssystem", "neurologische störungen","Endokrinologische Erkrankungen","Orthopädie"]
    
    section1 = [
        Section1Index(name: "Was ist Ohrakupunktur", forDetaiTextLabelInSection1: "Einführungen", forVideo: "Was brauch ich", beschreibung: "用耳朵上的穴位治疗疾病或者调理身体的阴阳平衡（亚健康就是阴阳失衡）的方法就叫“耳穴疗法”，较早的叫法是“耳针疗法”，简称“耳针”。而耳针中国古人称之为 “小针”、“微  针”或 “耳底神针”（这名字霸气！）耳针是祖国针灸的一个分支，即用针刺耳朵上的穴位来治病。因为针刺到耳朵上非常疼痛，一般老百姓都怕针痛，不便于推广，1975年洛阳的李佳琪发明了耳穴贴（又名耳穴压豆疗法）在一个小方块的胶带上放上一个王不留行籽（一种植物的种子），通过这个小且硬的东西，贴在耳朵上按压来刺激耳穴，达到同样的效果来代替耳针。从那之后耳针的名字逐渐退出。2002年在波多黎各召开的第四届世界耳针学学术大会上，“耳医学之母”黄丽春老前辈打败了在欧洲被誉为“现代耳医学之父”的法国人保罗.诺记尔（ Paul.Nogier ）勇夺第一名，获得终身研究成果奖。她经过40多年的研究，把耳针疗法、耳穴疗法发展成了耳医学。使耳针、耳穴疗法成为了一个单独的学科。该学科包含耳穴诊断和耳穴治疗，是一个完成的体系。并在美国成立了国际耳穴研究与培训中心（AMIRTC）。黄老师研究耳穴40多年，为耳穴的发展和弘扬做出了很大的贡献。"),
        Section1Index(name: "Wie mach ich das", forDetaiTextLabelInSection1: "Einführungen", forVideo: "Wie mach ich das", beschreibung: "So machen Sie das ..."),
        Section1Index(name: "Was brauch ich", forDetaiTextLabelInSection1: "Einführungen", forVideo: "Was brauch ich", beschreibung: "Das brauchen Sie ...")

    ]
    
    indication = [
        Indications(category: "Verdauungssystem", name: "Chronische Gastritis, Ulcus ventriculi und Ulcus duodeni", numOfPoint: "6-9", beschreibung: "Aus der Sicht der TCM Chronische\n中医认为生活起居方式außer ungesunder Lebensführung, falsche Ernährung, 或是先天的angeborener oder erworbener Schwächer des Magendarmsystems,是重要原因，Psychischen Streßfakturen 也spielt wichtig Faktor.中医认为情绪上因为长时间的 Emotionalen Dishamonier wie Sorger,Kummer,Trauer,Ärger,Frustration 会影响Leber qi Stagnation气滞，使得造成berwerken “肝火旺盛”，在中医的五行辩证中指出肝火旺盛会引起Chronische Gastritis, Ulcus ventriculi und Ulcus duodeni，所以中医主要以去肝火leber hitze治疗本病.", grundRezept: "主穴：Milz，Magen，Zwölffingerdarm,Leber,shenmen,皮质下.", rezeptModifikationen: "情绪不稳定 加 胆，三焦."),
        
        Indications(category: "Verdauungssystem", name: "Opstipation", numOfPoint: "8-12", beschreibung: "Opstipation早在公元前两百年，中医的Standardwerk der TCM “Die Innere Medizin des Kaisers” 已有详细的记载。Aus der Sicht der TCM.中医认为Psychischen Streßfakturen在这里spielt wichtig Faktor，Die Emotionalen Dishamonier wie Sorger,Kummer,Trauer,Ärger,Frustration 会造成肝气滞，长时间后造成肝火旺盛，在中医的五行辩证理论中Leberhitze verursachen 大肠的蠕动能力限制（Sie verursacht eine Motilitätstörung des Dickdarmes）,造成 腹部的绞痛和疼痛（abdominalen Verkampfung und Schmerzen）. Opstipation 在中医属于由于肝气质引起的实症热症Füller Typ, Hitze Typ.", grundRezept: "主穴：腹，大肠，乙状结肠，直肠，胃，脾，肺，三焦.", rezeptModifikationen: "大便干燥：加肺\n情绪不佳：肝，胆，神门."),
        
        Indications(category: "Verdauungssystem", name: "Diarrhö Durchfall", numOfPoint: "7-10", beschreibung: "Aus der Sicht der TCM 中医认为除了可能因为喜好生冷进食或食入不干净食物外，Die Emotionalen Dishamonier，和湿热困脾(Feucht und Hitze),也是其重要原因.", grundRezept: "主穴：直肠，大肠，神门，枕，脾，胃，交感", rezeptModifikationen: "配穴：腹泻次数增多： 加小肠\n过敏引起：加内分泌 和过敏区."),
        
        Indications(category: "Verdauungssystem", name: "Aufstoßen", numOfPoint: "7-8", beschreibung: "Aus der Sicht der TCM 中医早在公元前两百年，中医的Standardwerk der TCM “Die Innere Medizin des Kaisers” 已有详细的记载.Reizdarm 属于抑郁和肝气滞，本病与情绪和饮食习惯，饮食内容有很大的关系，如长期的Die Emotionalen Dishamonier wie Sorger,Kummer,Trauer,Ärger,Frustration 会造成肝气滞导致脏腑内气的运行不通而产生的各种症状.", grundRezept: "主穴：隔，胃，喷门，神们，交感，肝，脾.", rezeptModifikationen: "配穴：耳迷跟"),
        
        Indications(category: "Verdauungssystem", name: "Reizdarm", numOfPoint: "6-15", beschreibung: "Aus der Sicht der TCM 中医早在公元前两百年，中医的Standardwerk der TCM “Die Innere Medizin des Kaisers” 已有详细的记载.Reizdarm 属于抑郁和肝气滞，本病与情绪和饮食习惯，饮食内容有很大的关系，如长期的Die Emotionalen Dishamonier wie Sorger,Kummer,Trauer,Ärger,Frustration 会造成肝气滞导致脏腑内气的运行不通而产生的各种症状.", grundRezept: "主穴：胃，小肠，乙状结肠，交感，脾，皮质下.", rezeptModifikationen: "配穴：胃酸，嗳气，恶心，腹胀 肝，胆，腹胀区.\n头痛加:神门，枕.\n焦虑症加:身心穴."),
        
        Indications(category: "Verdauungssystem", name: "Ösophagitis", numOfPoint: "5-7", beschreibung: "中医早在公元前两百年，中医的Standardwerk der TCM “Die Innere Medizin des Kaisers” 已有详细的记载.\nAus der Sicht der TCM 中医认为食管炎在TCM认为是由于肺气虚，失去“肃降”（清洁，向下）的功能，肠胃气滞上逆，或肾气不足造成.\n肃：清肃指肺具有清除废浊之物的作用.\n降：降理解为降.", grundRezept: "主穴：食道，喷门，胃，皮质下，小肠.", rezeptModifikationen: "剧烈疼痛加: 神门，枕."),
        
        Indications(category: "Verdauungssystem", name: "Verdauungsstrun bei Kindern", numOfPoint: "7-10", beschreibung: "Aus der Sicht der TCM 小儿消化不良中医认为是由湿(Feucht oder Schleim)造成：湿可分为：寒湿Typ和湿热Typ, 这些痰湿阻挡了中焦，而致病（膲为体内脏器，是有形之物，上焦：心，肺。中焦：脾，胃，肝，胆。下焦：肾，大肠，小肠，膀胱).注意清淡的饮食，少食生冷，辛辣，油腻可以预防痰湿生成.", grundRezept: "主穴：胃，喷门，小肠，大肠，直肠，脾，皮质下.", rezeptModifikationen: "腹痛：腹，交感."),
        
        Indications(category: "Verdauungssystem", name: "呕吐，恶心", numOfPoint: "4-9", beschreibung: "包括晕车晕船，急性胃炎，壬辰呕吐.\nAus der Sicht der TCM\n中医认为呕吐，恶心属于风邪, 痰湿，食积，肝气滞，还有不正当的饮食习惯Wie 长期不良饮食习惯如饮食过量，生冷辛辣，肥甘不洁导致脾胃气不能降下.\n(风邪可分外风和内风。外风指空气流动而造成的气候，内风最主要由于肝功能失调，又称为感风内动).\n病例（FallBeispiel）：Frau D. G., 25 Jahre alt, Leher， 怀孕2月，每日清晨进食及呕吐，看见食物恶心呕吐，无胃病史，妇科检查为壬辰反应，药片吞咽不下，食后呕吐，用耳穴：胃，脾，神门，内分泌 ， 三日治疗后好转，1周后基本消失.", grundRezept: "主穴：胃，神门，交感，枕", rezeptModifikationen: "配穴：喷门，肝，脾，耳中，皮质下"),
        
        Indications(category: "Atmungssystem", name: "Hepatitis", numOfPoint: "8-11", beschreibung: "Die Heapatitis wurde bereits vor etwa 2200 Jahren in dem Standardwerk der TCM Die Innere Medizin des Kaisers ausführlich besprochen. Einige der darin erwähnten Rezepturen sind bis heute noch aktuell.\nAus der Sicht der TCM\n中医认为Nach Meinung der TCM können die emotionalen Konflikte wie sorge, Kummer, Trauer, Ärger und Zorn den krankheitsverlauf der Hepatiti verschlechtern.\n肝和神有着密切的关系，长期情绪不稳定可以肝气滞，使得肝阳 Überhitzung bzw in leber-Feuer，肝阳上炕导致eine Leberzellschädigung verursachen 或者影响已经的肝病患者的Heilungsprozeß.\n虽然在中医对本病理论再西医看来不逻辑不科学，但是在事件中他是有效的.（Obwohl diese alte Erkenntnis der TCM aus der sicht der Schulmedizin unlogisch und unwissenschaftlich erscheint, hat sie in der Alltagspraxis als richtig erwiesen.", grundRezept: "主穴：肝，胆，脾，三焦，内分泌，肾上腺，皮质下，耳中", rezeptModifikationen: "配穴： 肝区痛加 肝炎点，神门，交感"),
        
        Indications(category: "Atmungssystem", name: "感冒Grippaler Infekt", numOfPoint: "6-19", beschreibung: "Die Grippaler Infekt wurde bereits vor etwa 2200 Jahren in dem Standardwerk der TCM ‘Die Innere Medizin des Kaisers’ ausführlich besprochen.Aus der Sicht der TCM 中医认为天气(主要风邪)和自身抵抗能力下降是主要原因，起居失常Lebensverhaltungen,冷暖不调，淋雨过多，过度疲劳，酒后受风邪都可以导致机体能力下降。（Nach Meinung der TCM sind die kalte Witterung und die geschwächte Abwehrkraft des Körpers für diese Krankheit verantwortlich）风邪：可分外风和内风。外风指空气流动而造成的气候，内风最主要由于肝功能失调（funktionelle Störungen），又称为感风内动.", grundRezept: "主穴:肺，内鼻，咽喉，气管，额，肾上腺。", rezeptModifikationen: "配穴： 发热加耳尖\n头痛加交感，顳\n后头痛加枕，晕区\n头顶痛加肝，顶\n全身无力加肝，脾，三焦，快活点、\n咳嗽加气管，支气管"),
        
        Indications(category: "Atmungssystem ", name: "支气管哮喘Bronchialasthma ", numOfPoint: "6-12", beschreibung: "Das Bronchialasthma wurde bereits vor etwa 2200 Jahren in dem Standardwerk der TCM Die Innere Medizin des Kaisers ausführlich besprochen.\nAus der Sicht der TCM\n中医认为除了天气ungesunden Umwelteinflüssen和自身抵抗能力下降以外，因为肝和神有着密切的关系，长期情绪不稳定die emotionalen Konflikte wie sorge,Kummer,Trauer,Ärger und Zorn 可造成肝气滞.  因此由肝气郁滞造成的Psychischen Faktoren 是其重要原因.\n耳穴治疗对轻症治疗有较好效果，但是在急性发作时应采取和西医的综合治疗，在缓解期可以配合针灸，中药进行预防治疗，注意身心结合类的锻炼入太极，气功，注意清淡饮食，戒烟酒，戒辛辣，油腻.", grundRezept: "主穴:肺，支气管，对屏尖，肾上腺，平喘，内分泌", rezeptModifikationen: "配穴：喘息严重加 神门，交感，枕.\n呼吸困难者:加肾，胸，皮质下"),
        
        Indications(category: "Atmungssystem", name: "慢性支气管炎 Chronische Bronchitis", numOfPoint: "5-9", beschreibung: "Das Bronchialasthma wurde bereits vor etwa 2200 Jahren in dem Standardwerk der TCM <Die Innere Medizin des Kaisers> ausführlich besprochen.\nAus der Sicht der TCM\n中医认为除了天气ungesunden Umwelteinflüssen和自身抵抗能力下降是主要原因，耳穴治疗对轻症治疗有较好效果，但是在急性发作时应采取和西医的综合治疗，在缓解期可以配合针灸，中药进行预防治疗，注意身心结合类的锻炼入太极，气功，注意清淡饮食，戒烟酒，戒辛辣，油腻.", grundRezept: "主穴:神门，交感，肾，肝，枕", rezeptModifikationen: ""),
      ]
    
    
    
    if let splitViewController = splitViewController {
      let controllers = splitViewController.viewControllers
      detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
  }
    
    
   /* func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
 */
    
    /*func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont(name: "Oxygen-Bold", size: 38)!
    }
 */
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        let title = UILabel(frame: CGRect(x: 10, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        header.backgroundColor = UIColor.groupTableViewBackground
        title.font = UIFont(name: "Oxygen-Bold", size: 22)!
        title.textColor = UIColor.specialGreen
        title.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        title.sizeToFit()
        header.addSubview(title)
        return header
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section==0{
           return "Kurzeinführungen"
        }else{
            return "Indikationen"
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.specialGreen
        }
    }
    
  override func viewWillAppear(_ animated: Bool) {
    if splitViewController!.isCollapsed {
      if let selectionIndexPath = tableView.indexPathForSelectedRow {
        tableView.deselectRow(at: selectionIndexPath, animated: animated)
      }
    }
    super.viewWillAppear(animated)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
    
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0{
        return section1.count
    }else if isFiltering() {
      searchFooter.setIsFilteringToShow(filteredItemCount: filteredIndications.count, of: indication.count)
      return filteredIndications.count
    }
    searchFooter.setNotFiltering()
    return indication.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellID=["Cell2","Cell"][indexPath.section]
    let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
    let okData: Indications
    if indexPath.section==0{
        cell.textLabel?.text=section1[indexPath.row].name
        cell.detailTextLabel?.text=section1[indexPath.row].forDetaiTextLabelInSection1
        return cell
    }else if isFiltering(){
        okData = filteredIndications[indexPath.row]
    } else {
      okData = indication[indexPath.row]
    }
    cell.textLabel!.text = okData.name
    cell.detailTextLabel!.text = okData.category
        return cell
  }
  

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showDetail"{
      if let indexPath = tableView.indexPathForSelectedRow {
        let okData: Indications
        if isFiltering() {
          okData = filteredIndications[indexPath.row]
        } else {
          okData = indication[indexPath.row]
        }
        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
        controller.detailIndications = okData
        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        controller.navigationItem.leftItemsSupplementBackButton = true
      }
    }else if segue.identifier=="section2segue"{
        let s2v = (segue.destination as! UINavigationController).topViewController as! Section2ViewController
//            let section1IndexName = sender as! UITableViewCell
        if let indexPath = tableView.indexPathForSelectedRow{
            let oksection=section1[indexPath.row]
        
//        print(section1IndexName.textLabel?.text)
            s2v.detailSection1Index = oksection //your sender is a UITableViewCell
            }
        }
    }

  
  func filterContentForSearchText(_ searchText: String, scope: String = "All") {
    filteredIndications = indication.filter({( candy : Indications) -> Bool in
      let doesCategoryMatch = (scope == "All") || (candy.category == scope)
      
      if searchBarIsEmpty() {
        return doesCategoryMatch
      } else {
        return doesCategoryMatch && candy.name.lowercased().contains(searchText.lowercased())
      }
    })
    tableView.reloadData()
  }
  
  func searchBarIsEmpty() -> Bool {
    return searchController.searchBar.text?.isEmpty ?? true
  }
  
  func isFiltering() -> Bool {
    let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
    return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
  }
}
extension MasterViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
  }
}

extension MasterViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
    filterContentForSearchText(searchController.searchBar.text!, scope: scope)
  }
}
